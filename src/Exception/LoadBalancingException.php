<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;
use Throwable;

/**
 * Class LoadBalancingException
 * @package App\Exception
 */
class LoadBalancingException extends Exception
{
    /**
     * LoadBalancingException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "Host not found.", int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}