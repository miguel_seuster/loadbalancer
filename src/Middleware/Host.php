<?php
declare(strict_types=1);

namespace App\Middleware;

use http\Client\Request;

/**
 * Dummy class for IDE
 *
 * Class Host
 * @package App\Middleware
 */
class Host
{
    public function getLoad() :float {
        return (0 + (1 - 0) * (mt_rand() / mt_getrandmax()));
    }

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request) : void {
        //handle request
    }
}