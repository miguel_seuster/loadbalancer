<?php
declare(strict_types=1);

namespace App\Middleware;

use App\Exception\HostNotFoundException;
use App\Exception\LoadBalancingException;
use Exception;
use http\Client\Request;

/**
 * Class LoadBalancer
 * @package App\Middleware
 */
class LoadBalancer
{
    /**
     * @var Host[]
     */
    private $hosts;

    /**
     * @var string
     */
    private $balancing;

    /**
     * @var int
     */
    private $sequence = 0;

    /**
     * LoadBalancer constructor.
     *
     * @param Host[] $hosts
     * @param string $balancing
     */
    public function __construct(array $hosts, string $balancing)
    {
        $this->hosts = $hosts;
        $this->balancing = $balancing;
    }

    /**
     * @param Request $request
     *
     * @throws HostNotFoundException
     * @throws LoadBalancingException
     */
    public function handleRequest(Request $request)
    {
        try {
        $host = $this->balancing();
        $host->handleRequest($request);
        } catch (HostNotFoundException $hostNotFoundException) {
            throw $hostNotFoundException;
        } catch (Exception $exception) {
            throw new LoadBalancingException(sprintf("An error has occurred (%s)", $exception->getMessage()));
        }
    }

    /**
     * @return Host
     * @throws Exception
     */
    private function balancing(): Host
    {
        $host = null;
        switch ($this->balancing) {
            case "simple":
                $host = $this->getHostBySequence();
                break;
            case "load":
                $host = $this->getHostByLoad();
                break;
        }
        return $host;
    }

    /**
     * @return Host
     * @throws Exception
     */
    private function getHostBySequence()
    {
        try {
            $host = null;
            if (count($this->hosts) < $this->sequence) {
                $this->sequence = 0;
            }
            $host = $this->hosts[$this->sequence] ?? null;
            $this->sequence++;
            if (null == $host) {
                throw new HostNotFoundException("Could not get host by load.");
            }
            return $host;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return Host|null
     * @throws Exception
     */
    private function getHostByLoad()
    {
        try {
            $lowestLoadHost = null;
            foreach ($this->hosts as $host) {
                if ($host->getLoad() < 0.75) {
                    return $host;
                }
                if (null == $lowestLoadHost || $host->getLoad() < $lowestLoadHost->getLoad()) {
                    $lowestLoadHost = $host;
                }
            }
            if (null == $lowestLoadHost) {
                throw new HostNotFoundException("Could not get host by load.");
            }
            return $lowestLoadHost;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}